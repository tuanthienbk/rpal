#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_map>
#include <map>
#include <cctype>
#include <cmath>

using namespace std;

std::unordered_map<std::string,int> keywords = {
	{"let",1},{"in",2},{"fn",3},{"where",4},{"tau",5},{"aug",6},
	{"and",7},{"or",8},{"not",9},{"gr",10}, {"ge",11},{"ls",12},
	{"le",13},{"eq",14},{"ne",15},{"within",16},{"rec",17},{"true",18},
	{"false",19},{"nil",20},{"dummy",21} 
};
std::unordered_map<char,int> Operators = {
	{'+',1}, {'-',2}, {'*',3}, {'/',4}, {'@',5},{'&',6},{'|',7},
	{'%',8}, {'~',9},{':',10}, {'$',11}, {'!',12},{'#',13}, {'^',14},
	{'?',15}, {'=',16},{'>',17}, {'<',18}
};
std::unordered_map<char,int> delimiters = {
	{'.',1}, {'(',2}, {')',3}, {',',4}, {';',5},{'\'',6},{'\"',7},
	{'[',8},{']',9},{'_',10},{'{',11},{'}',12}
};

bool isKeyword(const string &s) {
	std::unordered_map<std::string,int>::const_iterator got = keywords.find (s);
	return got != keywords.end();
}
bool isOperator(const char &c) {
	std::unordered_map<char,int>::const_iterator got = Operators.find (c);
	return got != Operators.end();	
}
bool isDelimiter(char &c) {
	std::unordered_map<char,int>::const_iterator got = delimiters.find (c);
	return got != delimiters.end();
}

enum TokenType {
	IDENTIFIER, INTEGER, KEYWORD, OPERATOR, DELIMITER, STRING, LAMBDA, GAMMA, TAU, TUPLE, BETA, YSTAR, ETA, ENV, ENDOFFILE, UNKNOWN	
};
const std::string TOKEN_NAME[] = {"ID", "INT", "KEYWORD", "OPERATOR", "DELIMITER", "STR", "LAMBDA", "GAMMA", "TAU","TUPLE", "BETA", "Y*","ETA", "ENV","ENDOFFILE"};

struct Token {
	string str;
	TokenType type;
	vector<Token*> tuple;
	int tauCount;
	int envNum;
	int lambdaEnv;
	Token(string s, TokenType t): str(s), type(t) {} 
	Token& operator=(const Token& tk) {
		str = tk.str;
		type = tk.type;
		// tuple = tk.tuple;
		// tauCount = tk.tauCount;
		// envNum = tk.envNum;
		// lambdaEnv = tk.lambdaEnv;
		return *this;
	}
	Token(const Token& tk) {
		str = tk.str;
		type = tk.type;
	}
	void setValues(string s, TokenType t) {
		str = s;
		type = t;
	}
	bool isTuple(){
		return (str.find_first_of(",") != string::npos) || (type == TokenType::TUPLE);
	}
	friend ostream& operator << (ostream& os, Token &tk) {
		if (tk.type == TokenType::IDENTIFIER || tk.type == TokenType::INTEGER || tk.type == TokenType::STRING)
			os << "<"<<TOKEN_NAME[tk.type]<<":"<<tk.str<<">";
		else if ( tk.str == "true" || tk.str == "false" || tk.str == "nil" || tk.str == "dummy")
			os << "<"<<tk.str<<">";
		else 
			os << tk.str ;
		return os;
	}	
};

std::ifstream is; //global input stream
static int lineNum = 1; // global line number

int scan(Token *token) {
	char c;
	int i = 0;
	if (is.get(c)) {
		if (isspace(c)) {
			if (c == '\n' || c == '\r') lineNum++;
			return 1;
		}
		if (c == '/') {
			is.get(c);
			if (c == '/') {
				while (is.get(c) && c != '\n') {}
				lineNum++;
				return 1; 
			} else {
				is.seekg(-1,ios::cur);
				c = '/';
			}
		}
		if (isalpha(c)) {
			char buf[100];
			buf[0] = c;
			while (is.get(c)) {
				if (isalnum(c) || c == '_') 
					buf[++i] = c; 
				else {
					is.seekg(-1,ios::cur);
					break;
				}
			}
			buf[++i] = '\0';
			string word(buf,i);
			token->str = word;
			
			if (isKeyword(word)) {
				token->type = TokenType::KEYWORD;
			} else {
				token->type = TokenType::IDENTIFIER;
			}
			return 0;
		}
		if (isdigit(c)) {
			char buf[100];
			buf[0] = c;
			while (is.get(c)) {
				if (isdigit(c)) 
					buf[++i] = c; 
				else {
					is.seekg(-1,ios::cur);
					break;
				}
			}
			buf[++i] = '\0';
			string word(buf,i);
			token->str = word;
			token->type = TokenType::INTEGER;
			
			return 0;
		}
		if (ispunct(c)) {
			
			if (isDelimiter(c)) {
				if (c=='\'') {
					char buf[500];
					buf[0] = c;
					while (is.get(c) && c != '\'') {
						if (c=='\n') {
							std::cerr << "Line: " << lineNum <<": Incorrect string declaration " << std::endl;
							exit(0);
						}
						buf[++i] = c; 
					}
					buf[++i] = c; 
					buf[++i] = '\0';
					string word(buf,i);
					token->str = word;
					token->type = TokenType::STRING;
					return 0;
				}
				token->type = TokenType::DELIMITER;
				token->str = string(1,c);
				return 0;
			}
			else if (isOperator(c)) {
				token->type = TokenType::OPERATOR;
				if (c == '>') {
					is.get(c);
					if (c == '=') {
						token->str = ">=";
						return 0;
					}
					else {
						token->str = ">";
						is.seekg(-1,ios::cur);
						return 0;	
					}
				}
				else if (c == '<') {
					is.get(c);
					if (c == '=') {
						token->str = "<=";
						return 0;
					}
					else {
						token->str = "<";
						is.seekg(-1,ios::cur);
						return 0;	
					}	
				}
				else if (c == '*') {
					is.get(c);
					if (c == '*') {
						token->str = "**";
						return 0;
					}
					else {
						token->str = "*";
						is.seekg(-1,ios::cur);
						return 0;	
					}	
				} else if (c == '-') {
					is.get(c);
					if (c == '>') {
						token->str = "->";
						return 0;
					}
					else {
						token->str = "-";
						is.seekg(-1,ios::cur);
						return 0;	
					}	
				}
				else {
					token->str = string(1,c);
					return 0;
				}
			}
		} // punct
	}
	token->type = TokenType::ENDOFFILE;
	return 0;
}

struct Node {
	Token* token;
	Node* left;
	Node* right;
	Node(Token* tk): token(tk), left(NULL), right(NULL) {}
	Node(const Node& node) {
		token = node.token;
		left = node.left;
		right = node.right;
	}
	Node& operator=(const Node& node) {
		token = node.token;
		left = node.left;
		right = node.right;
		return *this;
	}
	friend ostream& operator << (ostream& os, Node &node) {
		os << *node.token;
		return os;
	}	
};

std::stack<Node*> AST; // global stack to build abstract structure tree

void Build_Tree(Token* token, int N) {
	if (N < 0) return;
	Node* n = new Node(token);
	if (N==0) {
		AST.push(n);
	} else if (AST.empty()) {
		return;
	} else {
		Node* t = AST.top();
		AST.pop();
		int i = 0;
		while (i < N-1 && !AST.empty())
		{
			Node* tt = AST.top(); AST.pop();
			tt->right = t;
			t = tt;
			i++;
		}
		n->left = t;
		AST.push(n);
	}
}

void PrintTree(Node* head, int depth) {
	for (int i = 0; i < depth; ++i)
		std::cout << "." ;
	std::cout <<  *head  << std::endl ;
	if (head->left != NULL) 
		PrintTree(head->left, depth+1);
	if (head->right != NULL)
	{
		PrintTree(head->right,depth);
	}
}

static Token *NT; // global next token

void Read(Token* tk){
	if (NT->str.compare(tk->str) != 0) {
		std::cerr << "Line "<<lineNum <<": Expected " << tk->str << " but found " << NT->str << std::endl;
		exit(0);
	}
	if (tk->type == TokenType::IDENTIFIER || tk->type == TokenType::INTEGER || tk->type == TokenType::STRING) {
		Token* token = new Token(*tk);
		Build_Tree(token,0);
	}
	while(scan(NT)) {}
}

void D();
void Vb();
void Ew();
void T();
void Ta();
void Tc();
void B();
void Bt();
void Bs();
void Bp();
void A();
void At();
void Af();
void Ap();
void R();
void Rn();
void Da();
void Dr();
void Db();
void Vl();
void Vb();
void E();

void E() {
	if (NT->type == TokenType::KEYWORD && NT->str.compare("let")==0) {
		Read(NT);
		D();
		Token* tk = new Token("in", TokenType::KEYWORD); Read(tk); delete tk; 
		E();
		Token* token = new Token("let", TokenType::KEYWORD);
		Build_Tree(token, 2);
	}
	else if (NT->type == TokenType::KEYWORD && NT->str.compare("fn")==0) {
		Read(NT);
		int N = 0;
		while(NT->type == TokenType::IDENTIFIER || NT->str.compare("(")==0) {
			Vb();
			N++;
		}
		Token* tk = new Token(".", TokenType::DELIMITER); Read(tk); delete tk;
		E();
		Token* token = new Token("lambda", TokenType::KEYWORD); 
		Build_Tree(token, N+1);
	} else {
		Ew();
	}
}
void Ew() {
	T();
	if (NT->type == TokenType::KEYWORD && NT->str.compare("where")==0) {
		Read(NT);
		Dr();
		Token* token = new Token("where", TokenType::KEYWORD); 
		Build_Tree(token, 2);
	}
}
void T() {
	Ta();
	int N = 0;
	while (NT->type == TokenType::DELIMITER && NT->str.compare(",")==0) {
		Read(NT);
		Ta();
		N++;
	}
	if (N > 0) {
		Token* token = new Token("tau", TokenType::KEYWORD);
		token->tauCount = N+1;
		Build_Tree(token,N+1);
	}	
}
void Ta(){
	Tc();
	while (NT->type == TokenType::KEYWORD && NT->str.compare("aug")==0) {
		Read(NT);
		Tc();
		Token* token = new Token("aug", TokenType::KEYWORD); 
		Build_Tree(token, 2);
	}
}
void Tc(){
	B();
	if (NT->type == TokenType::OPERATOR && NT->str.compare("->")==0) {
		Read(NT); 
		Tc();
		Token* tk = new Token("|", TokenType::OPERATOR); 
		Read(tk);
		Tc();
		Token* token = new Token("->", TokenType::OPERATOR); 
		Build_Tree(token, 3);
	}
}
void B() {
	Bt();
	while (NT->type == TokenType::KEYWORD && NT->str.compare("or")==0) { 
		Read(NT);
		Bt();
		Token* token = new Token("or", TokenType::KEYWORD); 
		Build_Tree(token, 2);
	}
}
void Bt(){
	Bs();
	while (NT->type == TokenType::OPERATOR && NT->str.compare("&")==0) {
		Read(NT); 
		Bs();
		Token* token = new Token("&", TokenType::OPERATOR); 
		Build_Tree(token, 2);
	}	
}
void Bs() {
	if (NT->type == TokenType::KEYWORD && NT->str.compare("not")==0) {
		Read(NT);
		Bp();
		Token* token = new Token("not", TokenType::KEYWORD); 
		Build_Tree(token, 1);
	} else {
		Bp();
	}
}
void Bp() {
	A();
	if ((NT->type == TokenType::KEYWORD && 
		(NT->str.compare("gr")==0 || NT->str.compare("ge")==0 || 
		 NT->str.compare("ls")==0 || NT->str.compare("le")==0 || 
		 NT->str.compare("eq")==0 || NT->str.compare("ne")==0) ) ||
		(NT->type == TokenType::OPERATOR && 
		(NT->str.compare(">")==0 || NT->str.compare(">=")==0 || 
		 NT->str.compare("<")==0 || NT->str.compare("<=")==0 )
		)) {
		Token *token = new Token(*NT);
		Read(NT);
		A();
		Build_Tree(token, 2);
	}
}
void A() {
	if (NT->type == TokenType::OPERATOR && NT->str.compare("-")==0) {
		Read(NT);
		At();
		Token* token = new Token("neg", TokenType::KEYWORD); 
		Build_Tree(token, 1);
	} else if (NT->type == TokenType::OPERATOR && NT->str.compare("+")==0) {
		Read(NT);
		At();
	} else {
		At();
	}
	while (NT->type == TokenType::OPERATOR && (NT->str.compare("-")==0 || NT->str.compare("+")==0)) {
		Token *token = new Token(*NT);
		Read(NT);
		At();
		Build_Tree(token,2);
	}
}
void At() {
	Af();
	while (NT->type == TokenType::OPERATOR && (NT->str.compare("/")==0 || NT->str.compare("*")==0)) {
		Token *token = new Token(*NT);
		Read(NT);
		Af();
		Build_Tree(token,2);
	}
}
void Af(){
	Ap();
	if (NT->type == TokenType::OPERATOR && NT->str.compare("**")==0) {
		Token *token = new Token(*NT);
		Read(NT);
		Af();
		Build_Tree(token,2);
	}
}
void Ap(){
	R();
	while (NT->type == TokenType::OPERATOR && NT->str.compare("@")==0) {
		Token *token = new Token(*NT);
		Read(NT);
		if (NT->type == TokenType::IDENTIFIER) {
			Read(NT);
			R();
			Build_Tree(token,3);
		}
	}
}
void R(){
	Rn();
	while ((NT->type == TokenType::IDENTIFIER || NT->type == TokenType::INTEGER || NT->type == TokenType::STRING) ||
		 (NT->type == TokenType::KEYWORD && (NT->str.compare("true")==0 || NT->str.compare("false")==0 || NT->str.compare("nil")==0 || NT->str.compare("dummy")==0) ) ||
		 (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0)
		 ) {
		Rn();
		Token* token = new Token("gamma", TokenType::GAMMA);
		Build_Tree(token,2); 
	}
}
void Rn(){
	if (NT->type == TokenType::IDENTIFIER || NT->type == TokenType::INTEGER || NT->type == TokenType::STRING) {
		Read(NT); 
	} else if (NT->type == TokenType::KEYWORD && (NT->str.compare("true")==0 || NT->str.compare("false")==0 
		|| NT->str.compare("nil")==0 || NT->str.compare("dummy")==0) ){
		// std::string str = "<"+NT->str+">";
		Token *token = new Token(NT->str,TokenType::KEYWORD);
		Read(NT);
		Build_Tree(token,0);
	} else if (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0) {
		Read(NT);
		E();
		Token* tk = new Token(")", TokenType::DELIMITER); Read(tk); delete tk;
	} else {
		std::cerr <<"Line " <<lineNum <<": Keyword is not supported" << std::endl;
		exit(0);
	}
}
void D() {
	Da();
	if (NT->type == TokenType::KEYWORD && NT->str.compare("within")==0) {
		Token *token = new Token(*NT);
		Read(NT);
		D();
		Build_Tree(token,2);
	}
}
void Da(){
	Dr();
	int N = 0;
	while (NT->type == TokenType::KEYWORD && NT->str.compare("and")==0) {
		Read(NT);
		Dr();
		N++;
	}
	if (N > 0) {
		Token* token = new Token("and", TokenType::KEYWORD);
		Build_Tree(token,N+1);
	}
}
void Dr() {
	Db();
	if (NT->type == TokenType::KEYWORD && NT->str.compare("rec")==0) {
		Token* token = new Token(*NT);
		Read(NT);
		Db();
		Build_Tree(token, 1);
	}
}
void Db(){
	if (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0)
	{
		Read(NT);
		D();
	} else if (NT->type == TokenType::IDENTIFIER) {
		Vl();
		if ( NT->type == TokenType::OPERATOR || NT->str.compare("=")==0) {
			Read(NT);
			Token* token = new Token("=", TokenType::OPERATOR);
			E();
			Build_Tree(token, 2); 
		} 
		else {
			int N = 1;
			while (NT->type == TokenType::IDENTIFIER || (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0)) {
				Vb();
				N++;
			}
			Token* tk = new Token("=", TokenType::DELIMITER); Read(tk); delete tk;
			E();
			Token* token = new Token("function_form", TokenType::KEYWORD);
			Build_Tree(token, N+1);
		}
	}
}
void Vb(){
	if ( NT->type == TokenType::IDENTIFIER )
	{
		Read(NT);
	} else if (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0) {
		Read(NT);
		if (NT->type == TokenType::DELIMITER && NT->str.compare(")")==0) {
			Token* token = new Token("()", TokenType::OPERATOR);
			Build_Tree(token, 0);
		} else {
			Vl();
			Token* tk = new Token(")", TokenType::DELIMITER); Read(tk); delete tk;
		}
	}
}
void Vl() {
	if (NT->type == TokenType::IDENTIFIER )
	{
		Read(NT);
		int N = 0;
		while (NT->type == TokenType::DELIMITER && NT->str.compare(",")==0) {
			Read(NT);
			if ( NT->type == TokenType::IDENTIFIER ) 
				Read(NT);
			else
				exit(0);
			N++;
		}
		if (N > 0) { 
			Token* token = new Token(",", TokenType::DELIMITER);
			Build_Tree(token, N+1);
		}
	}
}

/** Part 2*/

// First: Stardardize the AST tree
void Let(Node *top) {
	top->token->setValues("gamma", TokenType::GAMMA);	
    Node *eq = top->left;
    Node *p = eq->right;
    Node *x = eq->left;
    Node *e = x->right;
    eq->token->setValues("lambda", TokenType::LAMBDA);
    x->right = p;
    eq->right = e;
}
void Within(Node *top) {
	top->token->setValues("=",TokenType::OPERATOR);

    Node * eq1 = top->left;
    Node * eq2 = eq1->right;

    Node * x1 = eq1->left;
    Node * x2 = eq2->left;
    eq1->left = NULL;
    eq1->right = NULL;
    eq2->left = NULL;

    Node * e1 = x1->right;
    Node * e2 = x2->right;
    x1->right = NULL;
    x2->right = NULL;

    top->left = x2;
    x2->right = eq2;
    eq2->token->setValues("gamma",TokenType::GAMMA);
    eq2->left = eq1;
    eq1->token->setValues("lambda",TokenType::LAMBDA);
    eq1->right = e1;
    eq1->left = x1;
    x1->right = e2;
}
void Fcn(Node* top) {
	Node * p = top->left;
    Node * v = p->right;
    top->token->setValues("=",TokenType::OPERATOR);
    Node * lambda = new Node(new Token("lambda",TokenType::LAMBDA));
    lambda->left = v;
    p->right = lambda;
    Node * next = v->right;
    while (next->right != NULL)
    {
        Node * lam = new Node(new Token("lambda",TokenType::LAMBDA));
        v->right = lam;
        lam->left = next;
        v = next;
        next = v->right;
    }
}
void Lambda(Node * top) {
	Node * v = top->left;
    Node * next = v->right;
    while (next->right != NULL)
    {
        Node * lam = new Node(new Token("lambda",TokenType::LAMBDA));
        lam->left = next;
        v->right = lam;
        v = next;
        next = next->right;
    }
}
void And(Node *top) {
	Node * comma = new Node(new Token(",",TokenType::DELIMITER));
    Node * tau = new Node(new Token("tau",TokenType::TAU));
    comma->right = tau;
    Node * eq = top->left;
    Node * x = eq->left;
    Node * e = x->right;
    comma->left = x;
    tau->left = e;
    eq = eq->right;
    int N = 0;
    while (eq != NULL)
    {
        x->right = eq->left;
        x = x->right;
        e->right = eq->left->right;
        e = e->right;
        eq = eq->right;
        N++;
    }
    tau->token->tauCount = N + 1;
    x->right = NULL;
    top->token->setValues("=",TokenType::OPERATOR);
    top->left = comma;
}
void Infix(Node *top) {
	top->token->setValues("gamma",TokenType::GAMMA);
    Node * e1 = top->left;
    Node * n = e1->right;
    Node * e2 = n->right;
    Node * gam = new Node(new Token("gamma",TokenType::GAMMA));
    gam->left = n;
    gam->right = e2;
    top->left = gam;
    n->right = e1;
    e1->right = NULL;
}
void Where(Node *top){
	top->token->setValues("gamma",TokenType::GAMMA);
    Node * p = top->left;
    Node * eq = p->right;
    Node * x = eq->left;
    Node * e = x->right;
    eq->token->setValues("lambda",TokenType::LAMBDA);
    top->left = eq;
    x->right = p;
    p->right = NULL;
    eq->right = e;
}
void Rec(Node* top){
	top->token->setValues("=",TokenType::OPERATOR);
    Node * x = top->left->left;
    Node * e = x->right;
    delete top->left;
    top->left = x;
    Node * gam = new Node(new Token("gamma",TokenType::GAMMA));
    top->left->right = gam;
    gam->left =  new Node(new Token("<Y*>",TokenType::YSTAR));
    Node * lam = new Node(new Token("lambda",TokenType::LAMBDA));
    gam->left->right = lam;
    lam->left = new Node(x->token);
    lam->left->right = e;
}
void standardize(Node *top) {
	if (top == NULL)
        return;

    if (top->left != NULL)
        standardize(top->left);

    if (top->right != NULL)
        standardize(top->right);

    if (top->token->str.compare("let")==0)
    {
        Let(top);
    }

    else if (top->token->str.compare("within")==0)
    {
        Within(top);
    }

    else if (top->token->str.compare("function_form") ==0)
    {
        Fcn(top);
    }

    else if (top->token->str.compare("lambda")==0)
    {
        Lambda(top);
    }

    else if (top->token->str.compare("and")==0)
    {
        And(top);
    }

    else if (top->token->str.compare("@") ==0)
    {
        Infix(top);
    }

    else if (top->token->str.compare("where")==0)
    {
        Where(top);
    }

    else if (top->token->str.compare("rec")==0)
    {
        Rec(top);
    }
}
// Second: flatten the standardized AST tree
std::vector<std::vector<Token*> > deltaList;
std::queue<Node*> pendingDelta;
int deltaCounter = 0;

void preOrderTraversal(Node* root, std::vector<Token*>& currentDelta){
	if(root->token->str.compare("lambda")==0){
		if(root->left->token->str.compare(",") != 0){
			Token* tk = new  Token(root->left->token->str, TokenType::LAMBDA); tk->envNum = ++deltaCounter;
			currentDelta.push_back(tk);
		}else{
			Node* commaChild = root->left->left;
			string tuple;
			while(commaChild != NULL){
				tuple += commaChild->token->str;
				if (commaChild->right != NULL)  tuple += ",";
				commaChild = commaChild->right;
			}
			Token* tk = new Token(tuple, TokenType::LAMBDA); tk->envNum = ++deltaCounter;
			currentDelta.push_back(tk);
		}
		
		pendingDelta.push(root->left->right);
		if(root->right !=NULL)
			preOrderTraversal(root->right,currentDelta);
	} else if(root->token->str.compare("->")==0){
		Token* beta = new Token("beta",TokenType::BETA); 
		beta->envNum = ++deltaCounter;
		deltaCounter++;
		currentDelta.push_back(beta);
		
		pendingDelta.push(root->left->right);
		pendingDelta.push(root->left->right->right);

		root->left->right->right = NULL;
		root->left->right = NULL;
		
		if(root->left != NULL)
			preOrderTraversal(root->left,currentDelta);
		if(root->right !=NULL)
			preOrderTraversal(root->right,currentDelta);
	}else{
		currentDelta.push_back(root->token);
		if(root->left != NULL)
			preOrderTraversal(root->left,currentDelta);
		if(root->right !=NULL)
			preOrderTraversal(root->right,currentDelta);
	}
}

void flatten(Node *root) {
	pendingDelta.push(root);
	while(!pendingDelta.empty()){
		vector<Token*> currentDelta;
		Node* currStartNode = pendingDelta.front(); pendingDelta.pop();
		preOrderTraversal(currStartNode, currentDelta);
		deltaList.push_back(currentDelta);
	}
}
void printDeltaList(){
	for (int i = 0; i < deltaList.size(); ++i)
	{
		cout <<"delta"<<i<<":";
		for (int j = 0; j < deltaList[i].size(); ++j)
		{
			if (deltaList[i][j]->type == TokenType::LAMBDA)
				cout << deltaList[i][j]->str << "," << deltaList[i][j]->envNum <<" ";
			else
				cout << TOKEN_NAME[deltaList[i][j]->type]<<":"<< deltaList[i][j]->str << " ";
		}
		cout << endl;
	}
}

// Finally: evaluation using 13 rules
Token* applyOperator(Token* first, Token* second, Token* curr) {
	string op = curr->str;
	// if (op == "eq" && second->str =="'error'")
	// 	cout << endl;
	if(first->type == TokenType::INTEGER && second->type == TokenType::INTEGER){
		int firstVal = atoi(first->str.c_str());
		int secondVal = atoi(second->str.c_str());
		int resultVal = 0;
		if(op == "*"){
			resultVal = firstVal*secondVal;
			return new Token(to_string(resultVal),TokenType::INTEGER);
		}else if(op == "+"){
			resultVal = firstVal+secondVal;
			return new Token(to_string(resultVal),TokenType::INTEGER);
		}else if(op == "-"){
			resultVal = firstVal-secondVal;
			return new Token(to_string(resultVal),TokenType::INTEGER);
		}else if(op == "/"){
			resultVal = firstVal/secondVal;
			return new Token(to_string(resultVal),TokenType::INTEGER);
		}else if(op == "**"){
			resultVal = pow(firstVal,secondVal);
			return new Token(to_string(resultVal),TokenType::INTEGER);
		}else if(op == "gr"){
			return (firstVal > secondVal ? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "ls"){
			return (firstVal < secondVal ? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "ge"){
			return (firstVal >= secondVal ? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "le"){
			return (firstVal <= secondVal ? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "eq"){
			return (firstVal == secondVal ? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "ne"){
			return (firstVal != secondVal ? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}
	}else if(first->type == TokenType::STRING){ // String operators
		// if (first->str == "':='")
		// 	cout << endl; 
		if(op == "eq"){
			return (first->str == second->str ? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "ne"){
			return (first->str != second->str ? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}
	}else if(first->str == "true" || first->str == "false"){ // Boolean operators
		if(op == "or"){
			return ((first->str=="true" || second->str=="true")? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "&"){
			return ((first->str=="true" && second->str=="true")? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "eq"){
			return ((first->str==second->str)? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}else if(op == "ne"){
			return ((first->str != second->str)? new Token("true", TokenType::KEYWORD):new Token("false",TokenType::KEYWORD));
		}
	}
	return new Token("", TokenType::UNKNOWN);

}

std::unordered_map<std::string,int> binOps = {
	{"and",7},{"or",8},{"gr",10}, {"ge",11},{"ls",12},
	{"le",13},{"eq",14},{"ne",15},{"+", 1}, {"-", 2}, {"*", 3},
	{"/", 4}, {"**", 5}, {"&", 6}
};

bool isBinaryOp(const string &c) {
	std::unordered_map<string,int>::const_iterator got = binOps.find (c);
	return got != binOps.end();	
}
// std::unordered_map<std::string,int> buildInFcn = {
// 	{"stern",1},{"stem",2}, {"conc",3}, {"itos",4}, 
// 	{"print",5},{"Stern",6}, {"Isinteger",7}, {"Istruthvalue",8},
// 	{"Isstring",9}, {"Istuple",10}, {"Isdummy",11}, {"Isfunction",12},
// 	{"Order",13}, {"Stem",14}, {"Conc",15}, {"ItoS",16}, 
// 	{"Print",17}
// };
// bool isBuildin(const string &c) {
// 	std::unordered_map<string,int>::const_iterator got = buildInFcn.find (c);
// 	return got != buildInFcn.end();	
// }

vector<string> split(string inputString, char delimiter){
	vector<string> rs;
	std::istringstream iss(inputString);
	std::string token;
	while (std::getline(iss, token, delimiter))
	{
	    rs.push_back(token);
	}
	return rs;
}
// vector<Token*> toTuple(string inputString, char delimiter) {
// 	vector<Token*> rs;
// 	std::istringstream iss(inputString);
// 	std::string token;
// 	while (getline(iss, token, delimiter))
// 	{
// 	    rs.push_back(new Token(token,TokenType::STRING));
// 	}
// 	return rs;
// }
string unescape(const string& s)
{
  string res;
  string::const_iterator it = s.begin();
  while (it != s.end())
  {
    char c = *it++;
    if (c == '\\' && it != s.end())
    {
      switch (*it++) {
      case '\\': c = '\\'; break;
      case 'n': c = '\n'; break;
      case 't': c = '\t'; break;
      default:
        continue;
      }
    }
    res += c;
  }

  return res;
}
void printStack(stack<Token*> st) {
	while (!st.empty()) {
		Token* tk = st.top(); st.pop();
		cout << TOKEN_NAME[tk->type] << ":" << tk->str <<" ";
	}
}
int currEnv = 0;
int envCounter = 0;
std::map<pair<int,string>,Token*> paramMap;
std::map<int, int> envMap;
std::stack<int> envStack;
bool printCalled;

void processCurrentToken(Token* tk, stack<Token*>& cs, stack<Token*>& es) {
	if ( isBinaryOp (tk->str) ){
		Token* first = es.top(); es.pop();
		Token* second = es.top(); es.pop();
		Token* result = applyOperator(first, second, tk);
		es.push(result);
	} else if (tk->str == "neg"){
		Token* first = es.top(); es.pop();
		int paramVal = atoi(first->str.c_str());
		paramVal = -paramVal;
		Token* negToken = new Token(to_string(paramVal), TokenType::INTEGER);
		es.push(negToken);
	}else if(tk->str == "not"){
		Token* first = es.top(); es.pop();
		if(first->str == "true"){
			es.push(new Token("false",TokenType::KEYWORD));
		}else{
			es.push(new Token("true",TokenType::KEYWORD));
		}
	}else if(tk->type == TokenType::IDENTIFIER){
		string varName = tk->str;
		int temp = currEnv;
		// if (varName == "PIPE" && temp >= 12) 
		// 	cout << temp << endl;
		pair<int,string> keyPair(temp,varName);
		// if (varName =="C" && temp == 58) 
		// 	cout << keyPair.first<<":"<<varName << endl;	
		std::map<pair<int,string>,Token*>::iterator it = paramMap.find(keyPair);
		
		while(paramMap.end() == it && temp>=0 && temp != envMap[temp]){
			temp = envMap[temp];
			keyPair.first = temp;
			it = paramMap.find(keyPair);
		}
		if(paramMap.end() != it){
			Token* paramValToken = it->second;
			// if (paramValToken->type == TokenType::LAMBDA) paramValToken->lambdaEnv = currEnv;
			es.push(paramValToken);
			// if (varName =="C") cout << "->"<<*paramValToken << endl;			
		} else {
			es.push(tk);
			// cout <<"Cant find value for " << varName << endl;
		}

	} else if (tk->type == TokenType::GAMMA){
		Token* topExeToken = es.top(); es.pop();
		if(topExeToken->type == TokenType::LAMBDA){
			Token* env = new Token("env",TokenType::ENV); env->envNum = ++envCounter;
			envMap[envCounter] = topExeToken->lambdaEnv;
			// cout << "envMap[" << envCounter <<"]=" << topExeToken->lambdaEnv << endl;
			// if (envCounter == 13) 
			// 	cout << topExeToken->str << endl;
			envStack.push(envCounter);
			currEnv = envCounter;
			if(!topExeToken->isTuple()){
				string paramName = topExeToken->str;
				Token* paramToken = es.top(); es.pop();
				pair<int,string> keyPair(envCounter,paramName);
				paramMap[keyPair] = paramToken;
				// if (paramName == "C")
				// cout << "["<<envCounter<<","<<paramName<<"]->" << paramToken->str << endl;						
			}else{
				string tuple = topExeToken->str;
				vector<string> params = split(tuple,',');
				Token* valueTuple = es.top(); es.pop();
				vector<Token*> tupleVector = valueTuple->tuple;
				for(unsigned int i=0;i<params.size();i++){
					if(params[i] != ""){
						pair<int,string> keyPair(envCounter,params[i]);
						paramMap[keyPair] = tupleVector[i];
					}
				}
			}
			cs.push(env);
			es.push(env);
			for(int i=0;i<deltaList[topExeToken->envNum].size();i++) {
				cs.push(deltaList[topExeToken->envNum][i]);
			}
			
		}else if(topExeToken->type == TokenType::YSTAR){
			Token* nextToken = es.top(); es.pop();
			nextToken->type = TokenType::ETA;
			es.push(nextToken);
		}else if(topExeToken->type == TokenType::ETA){
			Token* lambdaToken = new Token(*topExeToken);
			
			lambdaToken->type = TokenType::LAMBDA; 
			lambdaToken->envNum = topExeToken->envNum;
			lambdaToken->lambdaEnv = topExeToken->lambdaEnv;

			es.push(topExeToken);
			es.push(lambdaToken);
			Token* gammaToken = new Token("gamma",TokenType::GAMMA);
			cs.push(gammaToken);
			cs.push(gammaToken);
		}else if(topExeToken->str == "Stern" || topExeToken->str == "stern"){
			Token* stringToken = es.top(); es.pop();
			string tokenValue = stringToken->str;
			tokenValue = tokenValue.substr(2,tokenValue.size()-3);
			tokenValue = "'"+tokenValue+"'";
			// stringToken->str = tokenValue;
			es.push(new Token(tokenValue, TokenType::STRING));
		}else if(topExeToken->str == "Stem" || topExeToken->str == "stem"){
			Token* stringToken = es.top(); es.pop();
			string tokenValue = stringToken->str;
			tokenValue = tokenValue.substr(1,1);
			tokenValue = "'"+tokenValue+"'";
			// stringToken->str = tokenValue;
			es.push(new Token(tokenValue,TokenType::STRING));
		}else if(topExeToken->str == "Conc" || topExeToken->str == "conc"){
			Token* firstToken = es.top();
			es.pop();
			Token* secondToken = es.top();
			es.pop();
			string concatValue = firstToken->str.substr(1,firstToken->str.size()-2)+secondToken->str.substr(1,secondToken->str.size()-2);
			concatValue = "'"+concatValue+"'";
			Token* newToken = new Token(concatValue,TokenType::STRING);
			es.push(newToken);
			cs.pop();
		}else if(topExeToken->str == "ItoS" || topExeToken->str == "itos"){
			Token* firstToken = es.top(); es.pop();
			firstToken->type = TokenType::STRING;
			string str = "'"+firstToken->str+"'";
			es.push(new Token(str,TokenType::STRING));			
		}else if(topExeToken->str == "Print" || topExeToken->str == "print"){
			printCalled = true;
			Token *t = es.top(); es.pop();
			if(!t->isTuple()){
				if(t->type == TokenType::STRING){
					string tempStr = unescape(t->str.substr(1,t->str.size()-2));
					cout << tempStr;
					if(tempStr[tempStr.size()-1] == '\n')
						cout<<endl;					
				}else if(t->type == TokenType::LAMBDA){
					cout <<"[lambda closure: "<<t->str<<": "<<t->envNum<<"]";
				}else{
					cout<<t->str;
				}
				Token* dummyToken = new Token("dummy",TokenType::KEYWORD);
				es.push(dummyToken);
			}else{
				vector<Token*> tupleVector = t->tuple;
				for(int i=0;i<tupleVector.size();i++){
					if(i==0){
						cout<<"(";
					}else{
						cout<<", ";
					}
					
					if(tupleVector[i]->type == TokenType::STRING){
						cout<< unescape(tupleVector[i]->str.substr(1,tupleVector[i]->str.size()-2));
					}else if(tupleVector[i]->isTuple()){
						// cout<<"Inside else if"<<endl;
						vector<Token*> innerTuple = tupleVector[i]->tuple;
						// cout << "Size" << innerTuple.size()<<endl;
						if(innerTuple.size() == 1){
							if(innerTuple[0]->type == TokenType::STRING)
								cout<< unescape(innerTuple[0]->str.substr(1,innerTuple[0]->str.size()-2));
						}
					}else{
						cout << tupleVector[i]->str;
					}
					
					if(i==tupleVector.size() -1){
						cout<<")";
					}
				}
			}
			// cout << endl;
		}else if(topExeToken->str == "Isinteger"){
			Token* t = es.top();
			es.pop();
			es.push(t->type==TokenType::INTEGER ? new Token("true",TokenType::KEYWORD): new Token("false",TokenType::KEYWORD));
		}else if(topExeToken->str == "Istruthvalue"){
			Token* t = es.top();
			es.pop();
			es.push(t->str=="true" || t->str=="false" ? new Token("true",TokenType::KEYWORD): new Token("false",TokenType::KEYWORD));
		}else if(topExeToken->str == "Isstring"){
			Token* t = es.top();
			es.pop();
			es.push(t->type==TokenType::STRING ? new Token("true",TokenType::KEYWORD): new Token("false",TokenType::KEYWORD));
		}else if(topExeToken->str == "Istuple"){
			Token* t = es.top();
			es.pop();
			es.push(t->isTuple() ? new Token("true",TokenType::KEYWORD): new Token("false",TokenType::KEYWORD));
		}else if(topExeToken->str == "Isdummy"){
			Token* t = es.top();
			es.pop();
			es.push(t->str=="dummy" ? new Token("true",TokenType::KEYWORD): new Token("false",TokenType::KEYWORD));
		}else if(topExeToken->str == "Isfunction"){
			Token* t = es.top();
			es.pop();
			es.push(t->type==TokenType::LAMBDA ? new Token("true",TokenType::KEYWORD): new Token("false",TokenType::KEYWORD));
		}else if(topExeToken->str == "Order"){
			Token* t = es.top();
			es.pop();
			vector<Token*> tupleVector = t->tuple;
			es.push(new Token(to_string(tupleVector.size()), TokenType::INTEGER));
		}else if(topExeToken->str == "Null"){
			Token* t = es.top();
			es.pop();
			es.push((t->str == "nil") ? new Token("true",TokenType::KEYWORD): new Token("false",TokenType::KEYWORD));
		}else if(topExeToken->isTuple()){
			Token* t = es.top();
			es.pop();
			vector<Token*> tupleVector = topExeToken->tuple;
			if(t->type == TokenType::INTEGER){
				int indx = atoi(t->str.c_str());
				indx -=1;
				es.push(tupleVector[indx]);
			}
		}
	} else if(tk->type == TokenType::ENV){
		Token* topToken = es.top();
		es.pop();
		es.pop();
		es.push(topToken);
		envStack.pop();
		currEnv = envStack.top();
		// if (currEnv==0)
		// 	cout << tk->envNum << endl;		
	}else if(tk->str == "beta"){
		Token* topToken = es.top();
		es.pop();
		// if (tk->envNum == 20)
		// 	cout << topToken->str << endl;
		vector<Token*> delta;
		if(topToken->str == "true"){
			delta = deltaList[tk->envNum];
		}else{
			delta = deltaList[tk->envNum+1];
		}
		for(unsigned int i=0;i<delta.size();i++){
			cs.push(delta[i]);
		}
	}else if(tk->str == "tau"){
		int tauCount = tk->tauCount;
		string tuple="(";
		vector<Token*> tupleVector;
		for(int i=0;i<tauCount;i++){
			Token* t = es.top();
			tupleVector.push_back(t);
			es.pop();
			if(i == tauCount -1)
				tuple += t->str;
			else
				tuple += t->str +", ";
		}
		tuple +=")";
		Token* newToken =  new Token(tuple,TokenType::TUPLE);
		newToken->tuple = tupleVector;
		es.push(newToken);
	}else if(tk->str == "nil"){
		tk->type = TokenType::TUPLE;
		es.push(tk);
	}else if(tk->str == "aug"){
		Token* tuple = es.top();
		es.pop();
		Token* toAdd = es.top();
		es.pop();
		if(tuple->str == "nil"){
			Token* newToken = new Token(toAdd->str,TokenType::TUPLE);
			if (toAdd->isTuple())
				newToken->tuple = toAdd->tuple;
			else
				newToken->tuple.push_back(toAdd);
			es.push(newToken);
		}else{
			tuple->tuple.push_back(toAdd);
			es.push(tuple);
		}
	}else if(tk->type == TokenType::LAMBDA){
		Token* nt = new Token(*tk);
		nt->lambdaEnv = currEnv;
		nt->envNum = tk->envNum;
		// tk->lambdaEnv = currEnv;
		es.push(nt);
	}else{
		es.push(tk);
	}	

}
void evaluation() {
	stack<Token*> CS; // control stack
	stack<Token*> ES; // evaluation stack
	Token *tk = new Token("env",TokenType::ENV);
	ES.push(tk);
	CS.push(tk);
	envStack.push(0);
	envMap[0] = -1;
	printCalled = false;
	for(int i=0;i<deltaList[0].size();i++) {
		CS.push(deltaList[0][i]);
	}
	while (CS.size() > 1 ) {
		Token* currToken = CS.top(); CS.pop();
		// cout << *currToken <<" " << currToken->envNum << endl;
		// if (currToken->envNum == 12)
			// cout << endl;
		processCurrentToken(currToken,CS,ES);
	}
	if (!printCalled) cout << endl;
}

int main(int argc, char* argv[])
{
	char* input_f;
	if (argc < 3) {
		input_f = argv[1];
	}
	else {
		input_f = argv[argc-1];	
	}
	is.open(input_f, ios::in);
	if (!is.is_open()) {
		std::cerr <<"Can't open the file " << std::endl;
		return 1;
	}
	NT = new Token("",TokenType::UNKNOWN);
	Read(NT);
	E();
	is.close();
	
	if (argc > 2) {
		PrintTree(AST.top(),0);
	}
	if (argc < 4) {
		standardize(AST.top());
		// PrintTree(AST.top(),0);
		flatten(AST.top());
		// printDeltaList();
		evaluation();
	}
	cout << flush;
 	return 0;
 }