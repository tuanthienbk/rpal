#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include <unordered_map>
#include <cctype>

using namespace std;

std::unordered_map<std::string,int> keywords = {
	{"let",1},{"in",2},{"fn",3},{"where",4},{"tau",5},{"aug",6},
	{"and",7},{"or",8},{"not",9},{"gr",10}, {"ge",11},{"ls",12},
	{"le",13},{"eq",14},{"ne",15},{"within",16},{"rec",17},{"true",18},
	{"false",19},{"nil",20},{"dummy",21} 
};
std::unordered_map<char,int> Operators = {
	{'+',1}, {'-',2}, {'*',3}, {'/',4}, {'@',5},{'&',6},{'|',7},
	{'%',8}, {'~',9},{':',10}, {'$',11}, {'!',12},{'#',13}, {'^',14},
	{'?',15}, {'=',16},{'>',17}, {'<',18}
};
std::unordered_map<char,int> delimiters = {
	{'.',1}, {'(',2}, {')',3}, {',',4}, {';',5},{'\'',6},{'\"',7},
	{'[',8},{']',9},{'_',10},{'{',11},{'}',12}
};

bool isKeyword(const string &s) {
	std::unordered_map<std::string,int>::const_iterator got = keywords.find (s);
	return got != keywords.end();
}
bool isOperator(const char &c) {
	std::unordered_map<char,int>::const_iterator got = Operators.find (c);
	return got != Operators.end();	
}
bool isDelimiter(char &c) {
	std::unordered_map<char,int>::const_iterator got = delimiters.find (c);
	return got != delimiters.end();
}

enum TokenType {
	IDENTIFIER, INTEGER, KEYWORD, OPERATOR, DELIMITER, STRING, ENDOFFILE, UNKNOWN	
};
const std::string TOKEN_NAME[] = {"ID", "INT", "KEYWORD", "OPERATOR", "DELIMITER", "STR","ENDOFFILE"};

struct Token {
	string str;
	TokenType type;
	Token(string s, TokenType t): str(s), type(t) {} 
	Token& operator=(const Token& tk) {
		str = tk.str;
		type = tk.type;
		return *this;
	}
	Token(const Token& tk) {
		str = tk.str;
		type = tk.type;
	}
	friend ostream& operator << (ostream& os, Token &tk) {
		os << tk.str <<": " << TOKEN_NAME[tk.type];
		return os;
	}	
};

std::ifstream is; //global input stream
static int lineNum = 1; // global line number

int scan(Token *token) {
	char c;
	int i = 0;
	if (is.get(c)) {
		if (isspace(c)) {
			if (c == '\n' || c == '\r') lineNum++;
			return 1;
		}
		if (c == '/') {
			is.get(c);
			if (c == '/') {
				while (is.get(c) && c != '\n') {}
				lineNum++;
				return 1; 
			} else {
				is.seekg(-1,ios::cur);
				c = '/';
			}
		}
		if (isalpha(c)) {
			char buf[100];
			buf[0] = c;
			while (is.get(c)) {
				if (isalnum(c) || c == '_') 
					buf[++i] = c; 
				else {
					is.seekg(-1,ios::cur);
					break;
				}
			}
			buf[++i] = '\0';
			string word(buf,i);
			token->str = word;
			
			if (isKeyword(word)) {
				token->type = TokenType::KEYWORD;
			} else {
				token->type = TokenType::IDENTIFIER;
			}
			return 0;
		}
		if (isdigit(c)) {
			char buf[100];
			buf[0] = c;
			while (is.get(c)) {
				if (isdigit(c)) 
					buf[++i] = c; 
				else {
					is.seekg(-1,ios::cur);
					break;
				}
			}
			buf[++i] = '\0';
			string word(buf,i);
			token->str = word;
			token->type = TokenType::INTEGER;
			
			return 0;
		}
		if (ispunct(c)) {
			
			if (isDelimiter(c)) {
				if (c=='\'') {
					char buf[500];
					buf[0] = c;
					while (is.get(c) && c != '\'') {
						if (c=='\n') {
							std::cerr << "Line: " << lineNum <<": Incorrect string declaration " << std::endl;
							exit(0);
						}
						buf[++i] = c; 
					}
					buf[++i] = c; 
					buf[++i] = '\0';
					string word(buf,i);
					token->str = word;
					token->type = TokenType::STRING;
					return 0;
				}
				token->type = TokenType::DELIMITER;
				token->str = string(1,c);
				return 0;
			}
			else if (isOperator(c)) {
				token->type = TokenType::OPERATOR;
				if (c == '>') {
					is.get(c);
					if (c == '=') {
						token->str = ">=";
						return 0;
					}
					else {
						token->str = ">";
						is.seekg(-1,ios::cur);
						return 0;	
					}
				}
				else if (c == '<') {
					is.get(c);
					if (c == '=') {
						token->str = "<=";
						return 0;
					}
					else {
						token->str = "<";
						is.seekg(-1,ios::cur);
						return 0;	
					}	
				}
				else if (c == '*') {
					is.get(c);
					if (c == '*') {
						token->str = "**";
						return 0;
					}
					else {
						token->str = "*";
						is.seekg(-1,ios::cur);
						return 0;	
					}	
				} else if (c == '-') {
					is.get(c);
					if (c == '>') {
						token->str = "->";
						return 0;
					}
					else {
						token->str = "-";
						is.seekg(-1,ios::cur);
						return 0;	
					}	
				}
				else {
					token->str = string(1,c);
					return 0;
				}
			}
		} // punct
	}
	token->type = TokenType::ENDOFFILE;
	return 0;
}

struct Node {
	Token* token;
	Node* left;
	Node* right;
	Node(Token* tk): token(tk), left(NULL), right(NULL) {} 
	friend ostream& operator << (ostream& os, Node &node) {
		os << node.token->str;
		return os;
	}	
};

std::stack<Node*> AST; // global stack to build abstract structure tree

void Build_Tree(Token* token, int N) {
	if (N < 0) return;
	Node* n = new Node(token);
	if (N==0) {
		AST.push(n);
	} else if (AST.empty()) {
		return;
	} else {
		Node* t = AST.top();
		AST.pop();
		int i = 0;
		while (i < N-1 && !AST.empty())
		{
			Node* tt = AST.top(); AST.pop();
			tt->right = t;
			t = tt;
			i++;
		}
		n->left = t;
		AST.push(n);
	}
}

void PrintTree(Node* head, int depth) {
	for (int i = 0; i < depth; ++i)
		std::cout << "." ;
	std::cout <<  *head  << std::endl ;
	if (head->left != NULL) 
		PrintTree(head->left, depth+1);
	if (head->right != NULL)
	{
		PrintTree(head->right,depth);
	}
}

static Token *NT; // global next token

void Read(Token* tk){
	if (NT->str.compare(tk->str) != 0) {
		std::cerr << "Line "<<lineNum <<": Expected " << tk->str << " but found " << NT->str << std::endl;
		exit(0);
	}
	if (tk->type == TokenType::IDENTIFIER || tk->type == TokenType::INTEGER || tk->type == TokenType::STRING) {
		std::string str = "<"+TOKEN_NAME[tk->type]+":"+tk->str+">";
		Token* token = new Token(str,TokenType::UNKNOWN);
		Build_Tree(token,0);
	}
	while(scan(NT)) {}
}

void D();
void Vb();
void Ew();
void T();
void Ta();
void Tc();
void B();
void Bt();
void Bs();
void Bp();
void A();
void At();
void Af();
void Ap();
void R();
void Rn();
void Da();
void Dr();
void Db();
void Vl();
void Vb();
void E();

void E() {
	if (NT->type == TokenType::KEYWORD && NT->str.compare("let")==0) {
		Read(NT);
		D();
		Token* tk = new Token("in", TokenType::KEYWORD); Read(tk); delete tk; 
		E();
		Token* token = new Token("let", TokenType::KEYWORD);
		Build_Tree(token, 2);
	}
	else if (NT->type == TokenType::KEYWORD && NT->str.compare("fn")==0) {
		Read(NT);
		int N = 0;
		while(NT->type == TokenType::IDENTIFIER || NT->str.compare("(")==0) {
			Vb();
			N++;
		}
		Token* tk = new Token(".", TokenType::DELIMITER); Read(tk); delete tk;
		E();
		Token* token = new Token("lambda", TokenType::KEYWORD); 
		Build_Tree(token, N+1);
	} else {
		Ew();
	}
}
void Ew() {
	T();
	if (NT->type == TokenType::KEYWORD && NT->str.compare("where")==0) {
		Read(NT);
		Dr();
		Token* token = new Token("where", TokenType::KEYWORD); 
		Build_Tree(token, 2);
	}
}
void T() {
	Ta();
	int N = 0;
	while (NT->type == TokenType::DELIMITER && NT->str.compare(",")==0) {
		Read(NT);
		Ta();
		N++;
	}
	if (N > 0) {
		Token* token = new Token("tau", TokenType::KEYWORD);
		Build_Tree(token,N+1);
	}	
}
void Ta(){
	Tc();
	while (NT->type == TokenType::KEYWORD && NT->str.compare("aug")==0) {
		Read(NT);
		Tc();
		Token* token = new Token("aug", TokenType::KEYWORD); 
		Build_Tree(token, 2);
	}
}
void Tc(){
	B();
	if (NT->type == TokenType::OPERATOR && NT->str.compare("->")==0) {
		Read(NT); 
		Tc();
		Token* tk = new Token("|", TokenType::OPERATOR); 
		Read(tk);
		Tc();
		Token* token = new Token("->", TokenType::OPERATOR); 
		Build_Tree(token, 3);
	}
}
void B() {
	Bt();
	while (NT->type == TokenType::KEYWORD && NT->str.compare("or")==0) { 
		Read(NT);
		Bt();
		Token* token = new Token("or", TokenType::KEYWORD); 
		Build_Tree(token, 2);
	}
}
void Bt(){
	Bs();
	while (NT->type == TokenType::OPERATOR && NT->str.compare("&")==0) {
		Read(NT); 
		Bs();
		Token* token = new Token("&", TokenType::OPERATOR); 
		Build_Tree(token, 2);
	}	
}
void Bs() {
	if (NT->type == TokenType::KEYWORD && NT->str.compare("not")==0) {
		Read(NT);
		Bp();
		Token* token = new Token("not", TokenType::KEYWORD); 
		Build_Tree(token, 1);
	} else {
		Bp();
	}
}
void Bp() {
	A();
	if ((NT->type == TokenType::KEYWORD && 
		(NT->str.compare("gr")==0 || NT->str.compare("ge")==0 || 
		 NT->str.compare("ls")==0 || NT->str.compare("le")==0 || 
		 NT->str.compare("eq")==0 || NT->str.compare("ne")==0) ) ||
		(NT->type == TokenType::OPERATOR && 
		(NT->str.compare(">")==0 || NT->str.compare(">=")==0 || 
		 NT->str.compare("<")==0 || NT->str.compare("<=")==0 )
		)) {
		Token *token = new Token(*NT);
		Read(NT);
		A();
		Build_Tree(token, 2);
	}
}
void A() {
	if (NT->type == TokenType::OPERATOR && NT->str.compare("-")==0) {
		Read(NT);
		At();
		Token* token = new Token("neg", TokenType::KEYWORD); 
		Build_Tree(token, 1);
	} else if (NT->type == TokenType::OPERATOR && NT->str.compare("+")==0) {
		Read(NT);
		At();
	} else {
		At();
	}
	while (NT->type == TokenType::OPERATOR && (NT->str.compare("-")==0 || NT->str.compare("+")==0)) {
		Token *token = new Token(*NT);
		Read(NT);
		At();
		Build_Tree(token,2);
	}
}
void At() {
	Af();
	while (NT->type == TokenType::OPERATOR && (NT->str.compare("/")==0 || NT->str.compare("*")==0)) {
		Token *token = new Token(*NT);
		Read(NT);
		Af();
		Build_Tree(token,2);
	}
}
void Af(){
	Ap();
	if (NT->type == TokenType::OPERATOR && NT->str.compare("**")==0) {
		Token *token = new Token(*NT);
		Read(NT);
		Af();
		Build_Tree(token,2);
	}
}
void Ap(){
	R();
	while (NT->type == TokenType::OPERATOR && NT->str.compare("@")==0) {
		Token *token = new Token(*NT);
		Read(NT);
		if (NT->type == TokenType::IDENTIFIER) {
			Read(NT);
			R();
			Build_Tree(token,3);
		}
	}
}
void R(){
	Rn();
	while ((NT->type == TokenType::IDENTIFIER || NT->type == TokenType::INTEGER || NT->type == TokenType::STRING) ||
		 (NT->type == TokenType::KEYWORD && (NT->str.compare("true")==0 || NT->str.compare("false")==0 || NT->str.compare("nil")==0 || NT->str.compare("dummy")==0) ) ||
		 (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0)
		 ) {
		Rn();
		Token* token = new Token("gamma", TokenType::KEYWORD);
		Build_Tree(token,2); 
	}
}
void Rn(){
	if (NT->type == TokenType::IDENTIFIER || NT->type == TokenType::INTEGER || NT->type == TokenType::STRING) {
		Read(NT); 
	} else if (NT->type == TokenType::KEYWORD && (NT->str.compare("true")==0 || NT->str.compare("false")==0 
		|| NT->str.compare("nil")==0 || NT->str.compare("dummy")==0) ){
		std::string str = "<"+NT->str+">";
		Token *token = new Token(str,TokenType::KEYWORD);
		Read(NT);
		Build_Tree(token,0);
	} else if (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0) {
		Read(NT);
		E();
		Token* tk = new Token(")", TokenType::DELIMITER); Read(tk); delete tk;
	} else {
		std::cerr <<"Line " <<lineNum <<": Keyword is not supported" << std::endl;
		exit(0);
	}
}
void D() {
	Da();
	if (NT->type == TokenType::KEYWORD && NT->str.compare("within")==0) {
		Token *token = new Token(*NT);
		Read(NT);
		D();
		Build_Tree(token,2);
	}
}
void Da(){
	Dr();
	int N = 0;
	while (NT->type == TokenType::KEYWORD && NT->str.compare("and")==0) {
		Read(NT);
		Dr();
		N++;
	}
	if (N > 0) {
		Token* token = new Token("and", TokenType::KEYWORD);
		Build_Tree(token,N+1);
	}
}
void Dr() {
	Db();
	if (NT->type == TokenType::KEYWORD && NT->str.compare("rec")==0) {
		Token* token = new Token(*NT);
		Read(NT);
		Db();
		Build_Tree(token, 1);
	}
}
void Db(){
	if (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0)
	{
		Read(NT);
		D();
	} else if (NT->type == TokenType::IDENTIFIER) {
		Vl();
		if ( NT->type == TokenType::OPERATOR || NT->str.compare("=")==0) {
			Read(NT);
			Token* token = new Token("=", TokenType::OPERATOR);
			E();
			Build_Tree(token, 2); 
		} 
		else {
			int N = 1;
			while (NT->type == TokenType::IDENTIFIER || (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0)) {
				Vb();
				N++;
			}
			Token* tk = new Token("=", TokenType::DELIMITER); Read(tk); delete tk;
			E();
			Token* token = new Token("function_form", TokenType::KEYWORD);
			Build_Tree(token, N+1);
		}
	}
}
void Vb(){
	if ( NT->type == TokenType::IDENTIFIER )
	{
		Read(NT);
	} else if (NT->type == TokenType::DELIMITER && NT->str.compare("(")==0) {
		Read(NT);
		if (NT->type == TokenType::DELIMITER && NT->str.compare(")")==0) {
			Token* token = new Token("()", TokenType::OPERATOR);
			Build_Tree(token, 0);
		} else {
			Vl();
			Token* tk = new Token(")", TokenType::DELIMITER); Read(tk); delete tk;
		}
	}
}
void Vl() {
	if (NT->type == TokenType::IDENTIFIER )
	{
		Read(NT);
		int N = 0;
		while (NT->type == TokenType::DELIMITER && NT->str.compare(",")==0) {
			Read(NT);
			if ( NT->type == TokenType::IDENTIFIER ) 
				Read(NT);
			else
				exit(0);
			N++;
		}
		if (N > 0) { 
			Token* token = new Token(",", TokenType::DELIMITER);
			Build_Tree(token, N+1);
		}
	}
}

int main(int argc, char* argv[])
{
	char* input_f;
	if (argc < 3) {
		input_f = argv[1];
	}
	else {
		input_f = argv[2];	
	}
	is.open(input_f, ios::in);
	if (!is.is_open()) {
		std::cerr <<"Can't open the file " << std::endl;
		return 1;
	}
	NT = new Token("",TokenType::UNKNOWN);
	Read(NT);
	E();
	is.close();
	
	if (argc > 2) {
		PrintTree(AST.top(),0);
	}
	return 0;
 }